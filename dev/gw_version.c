/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2016
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

/*
 * Library to read the info from the GW_VERSION module.
 */
#include <wrc.h>
#include "gw_version.h"

/* Command to write into the SPI controller registers through WB */
static inline void wb_gw_ver_writel(uint32_t data, uint32_t reg)
{
	*(volatile uint32_t *)(BASE_GW_VERSION + reg) = data;		//Instruction to write at physical memory
}

/* Command to read from the SPI controller registers through WB */
static inline uint32_t wb_gw_ver_readl(uint32_t reg)
{
	return *(volatile uint32_t *)(BASE_GW_VERSION + reg);		//Instruction to read from the physical memory
}

/* It prints the gw info. */
void print_gw_info(void)
{
	uint32_t board_id;
	uint32_t major_minor_ver;
	uint32_t major, minor, build;
	uint32_t date_ver;
	uint32_t min,hour,day,month,year;

	// GW_VER[0x0]: BOARD_ID (32b)
	board_id = wb_gw_ver_readl(GW_REG_BOARD_ID);;

	// GW_VER[0x1]: VER_MAJ (8b) | VER_MIN (8b) | VER_BUILD (16b)
	major_minor_ver = wb_gw_ver_readl(GW_REG_MINOR_VER);

	// GW_VER[0x2]: BOARD_VER (32b)
	date_ver = wb_gw_ver_readl(GW_REG_DATE_VER);

	major = (major_minor_ver >> 24) & 0xf;
	minor = (major_minor_ver >> 16) & 0xf;
	build = (major_minor_ver & 0xff);

	min = date_ver & 0x3f;
	hour = (date_ver >> 6) & 0x1f;
	day = (date_ver >> 11) & 0x1f;
	month = (date_ver >> 16) & 0x0f;
	year = (date_ver >> 20) & 0x7f;

	//printf("RAW: board_id: %x, major_min: %x, date: %x \n",board_id,major_minor_ver,date_ver);

	pp_printf("\nGateware info: \n");
	pp_printf(" GW ID      : 0x%08x\n",board_id);
	pp_printf(" GW VERSION : %d.%d.%d\n",major,minor,build);
	pp_printf(" GW DATE    : %02d/%02d/%02d %02d:%02d\n",year,month,day,hour,min);
}
