obj-y += \
	dev/endpoint.o \
	dev/ep_pfilter.o \
	dev/i2c.o \
	dev/minic.o \
	dev/pps_gen.o \
	dev/syscon.o \
	dev/sfp.o \
	dev/sdb.o \
	dev/rxts_calibrator.o \
	dev/gw_version.o \
	dev/pca9554.o

obj-$(CONFIG_DIO) += 			dev/dio.o 
obj-$(CONFIG_LENTRIG) +=	dev/len_fine_tune.o
obj-$(CONFIG_LEGACY_EEPROM) += 	dev/eeprom.o
obj-$(CONFIG_SDB_EEPROM) += 	dev/sdb-eeprom.o
obj-$(CONFIG_IRIGB) += 			dev/irigb_sync.o
obj-$(CONFIG_ETHPORT) +=		dev/eth_subsystem.o dev/mini_switch_core.o
obj-$(CONFIG_W1) +=				dev/w1.o dev/w1-hw.o	dev/w1-shell.o
obj-$(CONFIG_W1) +=				dev/w1-temp.o	dev/w1-eeprom.o
obj-$(CONFIG_UART) +=			dev/uart.o
obj-$(CONFIG_UART_SW) +=		dev/uart-sw.o
obj-$(CONFIG_ZEN_BOARD) += 		dev/wr_zen_i2c.o
obj-$(CONFIG_ZEN_CTA_BOARD) += 	dev/wr_zen_cta_gpio.o
obj-$(CONFIG_EXPANSION_SPI) += 	dev/exp_spi.o
obj-$(CONFIG_XADC_TEMP) += dev/xadc_temp.o
