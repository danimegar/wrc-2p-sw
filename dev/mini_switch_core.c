/*
 * This work is part of the WR-LEN project
 *
 * Author: Emilio Marín-López <emilio<at>sevensols.com>
 *
 */


#include <stdio.h>
#include <string.h>
#include <wrc.h>
#include "syscon.h"

#include "hw/miniswch_regs.h"

volatile struct MINISWCH_WB *MINISWCH;

#define LINK_WENT_UP 1
#define LINK_WENT_DOWN 2
#define LINK_UP 3
#define LINK_DOWN 4


//Info about the neighbor
int nb_mac_learned = 0;
uint8_t nb_addr[6];

void ms_init(){
	MINISWCH = (volatile struct MINISWCH_WB *)BASE_WB_MINISWCH;
	mprintf("Mini-Switch module has been enabled \n");
}

void ms_eth_print_rtu(){

	uint8_t mac_to_print[6];
	uint8_t age_to_print;
	uint8_t num_ep_entries[wrlen_num_ports]= {-1 ,-1};
	uint32_t mac_lo, mac_hi;
	int i;

	//request to fill the FIFO queue with the RTU info.
	MINISWCH->MNG_RTU = MINISWCH_MNG_RTU_FILL_REQ;

	if( nb_mac_learned == 1 ){
		/* Neighbor MAC address */
		pp_printf("Neighbor MAC address:	%x:%x:%x:%x:%x:%x\n\n", nb_addr[0],
				nb_addr[1], nb_addr[2], nb_addr[3], nb_addr[4], nb_addr[5]);
	} else
		pp_printf("Neighbor MAC address:	No MAC\n\n");


	//Num of entries stored in both EP FIFOS.
	num_ep_entries[0] = (uint8_t)MINISWCH_EP0_FIFO_CSR_USEDW_R(MINISWCH->EP0_FIFO_CSR);
	num_ep_entries[1] = (uint8_t)MINISWCH_EP1_FIFO_CSR_USEDW_R(MINISWCH->EP1_FIFO_CSR);

	//Endpoint 0 FIFO
	pp_printf("EP0 entries:	%i\n\n" , num_ep_entries[0]);
	for(i = 0; i< num_ep_entries[0]; i++ ){

		mac_hi = MINISWCH->EP0_FIFO_R0;
		mac_lo = MINISWCH->EP0_FIFO_R1;
		age_to_print = MINISWCH_EP0_FIFO_R2_MAC_AGE_R(MINISWCH->EP0_FIFO_R2);

		// mac
		mac_to_print[5]   = 0xFF &  mac_lo;
		mac_to_print[4]   = 0xFF & (mac_lo >> 8);
		mac_to_print[3]   = 0xFF & (mac_lo >> 16);
		mac_to_print[2]   = 0xFF & (mac_lo >> 24);
		mac_to_print[1]   = 0xFF &  mac_hi;
		mac_to_print[0]   = 0xFF & (mac_hi >> 8);

		pp_printf("	%i:	MAC: %x:%x:%x:%x:%x:%x	",i,  mac_to_print[0],
				mac_to_print[1], mac_to_print[2], mac_to_print[3], mac_to_print[4], mac_to_print[5]);
		pp_printf("Age: %u		", age_to_print);
		pp_printf("EP0\n");
	}
	pp_printf("\n");

	//Endpoint 1 FIFO.
	pp_printf("EP1 entries:	%i\n\n" , num_ep_entries[1]);
	for(i = 0; i< num_ep_entries[1]; i++ ){

		mac_hi = MINISWCH->EP1_FIFO_R0;
		mac_lo = MINISWCH->EP1_FIFO_R1;
		age_to_print = MINISWCH_EP1_FIFO_R2_MAC_AGE_R(MINISWCH->EP1_FIFO_R2);

		//mac
		mac_to_print[5]   = 0xFF &  mac_lo;
		mac_to_print[4]   = 0xFF & (mac_lo >> 8);
		mac_to_print[3]   = 0xFF & (mac_lo >> 16);
		mac_to_print[2]   = 0xFF & (mac_lo >> 24);
		mac_to_print[1]   = 0xFF &  mac_hi;
		mac_to_print[0]   = 0xFF & (mac_hi >> 8);

		pp_printf("	%i:	MAC: %x:%x:%x:%x:%x:%x	",i,  mac_to_print[0],
				mac_to_print[1], mac_to_print[2], mac_to_print[3], mac_to_print[4], mac_to_print[5]);
		pp_printf("Age: %u		", age_to_print);
		pp_printf("EP1\n");
	}

}

void ms_reset_rtu_ep(int port_num){

	uint32_t initial_reg = MINISWCH->MNG_RTU;

	//Reset the EP0 RTU.
	if (port_num == 0)
		MINISWCH->MNG_RTU = MINISWCH_MNG_RTU_RST_EP0;

	//Reset the EP0 RTU.
	if (port_num == 1)
		MINISWCH->MNG_RTU = MINISWCH_MNG_RTU_RST_EP1;

	//Reset both RTUs
	if (port_num == 2)
		MINISWCH->MNG_RTU = MINISWCH_MNG_RTU_RST_EP0 | MINISWCH_MNG_RTU_RST_EP0;

	MINISWCH->MNG_RTU = initial_reg;
}


void ms_eth_update(){

	int eth_link_state = eth_link_detection();

	if ((eth_link_state == LINK_UP) && (nb_mac_learned == 0)) {
		if ((MINISWCH->NB_CRTL & MINISWCH_NB_CRTL_NB_MAC_VALID) == MINISWCH_NB_CRTL_NB_MAC_VALID){

			//Load the NB_MAC into the ep_filtes for both ports.
			pfilter_len_switch_config(0);
			pfilter_len_switch_config(1);

			nb_addr[5] = (MINISWCH->NB_MACL & 0x000000ff);
			nb_addr[4] = (MINISWCH->NB_MACL & 0x0000ff00) >> 8;
			nb_addr[3] = (MINISWCH->NB_MACL & 0x00ff0000) >> 16;
			nb_addr[2] = (MINISWCH->NB_MACL & 0xff000000) >> 24;
			nb_addr[1] = (MINISWCH->NB_MACH & 0x000000ff);
			nb_addr[0] = (MINISWCH->NB_MACH & 0x0000ff00) >> 8;

			/* MAC address */
			pp_printf("Neighbor MAC discovered:	%x:%x:%x:%x:%x:%x\n", nb_addr[0],
					nb_addr[1], nb_addr[2], nb_addr[3], nb_addr[4], nb_addr[5]);

			pp_printf("New forwarding rules loaded. \n");

			nb_mac_learned = 1;
		}
	} else if (eth_link_state == LINK_WENT_DOWN){
		//The fsm inside the miniswitching core must be reset from software.
		MINISWCH->NB_CRTL = MINISWCH_NB_CRTL_NB_MAC_RST;
		MINISWCH->NB_CRTL = ~MINISWCH_NB_CRTL_NB_MAC_RST;

		//Loading the old ep_filter rules.
		pfilter_len_default(0);
		pfilter_len_default(1);
		pp_printf("Original forwarding rules loaded. \n");
		nb_mac_learned = 0;
	}
}
