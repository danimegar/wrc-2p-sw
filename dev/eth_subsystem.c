/*
 * This work is part of the WR-LEN project
 *
 * Author: Emilio Marín López <emilio<at>sevensols.com>
 *
 */

#include <stdio.h>
#include <string.h>
#include <wrc.h>
#include "syscon.h"

#include <endpoint.h>

#include "eth_subsystem.h"

#define LINK_WENT_UP 1
#define LINK_WENT_DOWN 2
#define LINK_UP 3
#define LINK_DOWN 4

volatile void *ETHSUB;

uint8_t phy_address;

static inline void ethsubsystem_write(uint32_t reg, uint32_t data)
{
	*(volatile uint32_t *)(ETHSUB + reg) = data;		//Instruction to write at physical memory
}

static inline uint32_t ethsubsystem_read(uint32_t reg)
{
	return *(volatile uint32_t *)(ETHSUB + reg);		//Instruction to read from the physical memory
}

/*
 * These functions set the proper configuration into the Ethernet_System memory map.
 */

int eth_link_detection(){
	static int prev_link_state = LINK_DOWN;
	uint32_t read_reg;
	int link_state = 0;

	int rv = 0;

	//Read from the MDIO register. The first read is random.
	read_reg = ethsubsystem_read( 0x50c );

	//Check if the link is up.
	if ( read_reg & 1<<2 )
		link_state = 1;

	if (!prev_link_state && link_state) {
		mprintf("ETH: Link up.");
		eth_restart_autoneg();
		rv = LINK_WENT_UP;
	} else if (prev_link_state && !link_state) {
		mprintf("ETH: Link down.\n");
		rv = LINK_WENT_DOWN;
	} else
		rv = (link_state ? LINK_UP : LINK_DOWN);

	prev_link_state = link_state;

	//The Status MDIO register is prepared to be read.
	ethsubsystem_write( 0x504 , 0x00018800  | (phy_address << 24) );

	return rv;
}

void reset_transceiver(){

    //Reset KSZ9031 via MDIO.
	ethsubsystem_write( 0x508 , 0x00008000);
	ethsubsystem_write( 0x504 , 0x00004800 | (phy_address << 24));
}

/*
* Adjusting the ETH KSZ9031 hold and setup times.
*/

void adjust_hold_setup_times(){

	//Select MMD device address 2h
	ethsubsystem_write( 0x508, 0x00000002 );
	timer_delay(100);
	ethsubsystem_write( 0x504, 0x000D4880 | (phy_address << 24));
	timer_delay(100);

	// Select Register 8h of Device Address 2h
	ethsubsystem_write( 0x508, 0x00000008 );
	timer_delay(100);
	ethsubsystem_write( 0x504, 0x000E4880 | (phy_address << 24));
	timer_delay(100);

	//Select register data for MMD Device Address 2h, Register 8h
	ethsubsystem_write( 0x508, 0x00004002 );
	timer_delay(100);
	ethsubsystem_write( 0x504, 0x000D4880 | (phy_address << 24));
	timer_delay(100);

	//Write value 0x03FF to MMD Device Address 2h, Register 8h
	ethsubsystem_write( 0x508, 0x00000170 );
	timer_delay(100);
	ethsubsystem_write( 0x504, 0x000E4880 | (phy_address << 24));
}

/*
 * Function to initialize the KSZ9031 ethernet transceiver..
 */
void eth_subsystem_init(){

	uint32_t read_reg;
	uint8_t cntr = 0;
	phy_address = 0x0;

	ETHSUB = (volatile void *)BASE_WB_ETHSUB;

	//MDIO: Enabled, MDC = 1.25 MHz (CLK_DIVIDE = 0x18)
	ethsubsystem_write( 0x500 , 0x00000058 );

	//Detecting the KSZ9031 physical address by polling.
	for (cntr = 0 ; cntr < 8 ; cntr++) {

	    // Read MDIO Status
	    ethsubsystem_write(0x504 , 0x00028800 | (cntr << 24));

	    //Delay to enable the MDIO register readout.
	    timer_delay(100);
	    read_reg = ethsubsystem_read(0x50c);

	    //The transceiver has been detected.
	    if ( read_reg == 0x10022 )
	    	phy_address = cntr;
	}

	//Reset the EthSubsystem (Rx & Tx). These lines block the Ethernet.
	ethsubsystem_write( 0x404 , 0x80000000 );
	timer_delay(100);
	ethsubsystem_write( 0x408 , 0x80000000 );
	timer_delay(100);

	//Pause Frame MAC addrs.
	ethsubsystem_write( 0x400 , 0xddccbbaa );

	//RX Jumbo frames enabled.
	ethsubsystem_write( 0x404 , 0x5000ffee );

	//TX Jumbo_Frames enabled.
	ethsubsystem_write( 0x408 , 0x50000000 );

	//The transceiver is reset.
	reset_transceiver();

	// MAC = AA:BB:CC:DD:EE:FF
	ethsubsystem_write( 0x20 , 0xddccbbaa );
	ethsubsystem_write( 0x24 , 0x0000ffee );

	// (MDIO 0h) 1000Mbps, AN enabled, full-duplex. (00003100)
	//ethsubsystem_write( 0x508 , 0x00003100 );
	ethsubsystem_write( 0x508 , 0x00001300 );
	ethsubsystem_write( 0x504 , 0x00004800 | (phy_address << 24));

	// 1000Mbps (a) , RGMII, TX16 & RX16
	ethsubsystem_write( 0x410, 0xa3000000 );	//1000 Mbps by default.

    // Read MDIO Status
	ethsubsystem_write(0x504 , 0x00028800 | (phy_address << 24));

    //Delay to enable the MDIO register readout.
    timer_delay(200);
    read_reg = ethsubsystem_read(0x50c);

    if ( read_reg == 0x10022 ) {
    	mprintf( "KSZ9031 detected \n"  );
    	mprintf( "MDIO PHY Address = 0x%2x  \n", phy_address  );
	} else
    	mprintf( "KSZ9031 is not detected. Reg = %u \n", read_reg);

    //Adjust the hold and setup times.
    adjust_hold_setup_times();
}

/*
 * Function to restart the auto negotiation process.
 * It is called every time the eth link is detected.
 */
void eth_restart_autoneg(){
	uint32_t read_reg;

	ethsubsystem_write( 0x508 , 0x00001300 );
	ethsubsystem_write( 0x504 , 0x00004800 | (phy_address << 24) );	//Register 0 (Write)

    //Speed detection.
	ethsubsystem_write( 0x504 , 0x001F8800 | (phy_address << 24));	//Register 1F (Read PHY Control)
	timer_delay(100);
	read_reg = ethsubsystem_read(0x50c);

	if ( ( 0x1 & ( read_reg >> 6 )) == 1 ){
		ethsubsystem_write( 0x410, 0xa3000000 );	// To set 1000 Mbps.
		mprintf("1000Base-T \n");
	} else if ( ( 0x1 & ( read_reg >> 5 )) == 1 ){
		ethsubsystem_write( 0x410, 0x63000000);		// To set 100 Mbps
		mprintf("100Base-T \n");
	} else if ( ( 0x1 & ( read_reg >> 4 )) == 1 ){
		ethsubsystem_write( 0x410, 0x23000000);		// To set 10 Mbps
		mprintf("10Base-T \n");
	}
}
