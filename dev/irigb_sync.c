/*
 * irigb_sync.c
 *
 *  Created on: Oct 24, 2014
 *      Author: Huse Gomiz
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <wrc.h>
#include "types.h"
#include <stdio.h>
#include "irigb_sync.h"

#include "pps_gen.h"		/* for pps_gen_get_time() */
#include "hw/irig_b_wishbone.h"

#include "util.h"

volatile struct IRIGB_WB *IRIGB;

/*
#define IRIG_B_RST_ENA 0x00000000
#define IRIG_B_YEAR 0x00000004
#define IRIG_B_DAY 0x00000008
#define IRIG_B_HOUR 0x0000000c
#define IRIG_B_MIN 0x00000010
#define IRIG_B_SEC 0x00000014
#define IRIG_B_CAFE 0x00000018 */

#define YEAR0   1900
#define EPOCH_YR   1970
#define SECS_DAY   (24L * 60L * 60L)
#define LEAPYEAR(year)   (!((year) % 4) && (((year) % 100) || !((year) % 400)))
#define YEARSIZE(year)   (LEAPYEAR(year) ? 366 : 365)

struct irig_tm
{
  int tm_sec;			/* Seconds.	[0-60] (1 leap second) */
  int tm_min;			/* Minutes.	[0-59] */
  int tm_hour;			/* Hours.	[0-23] */
  int tm_year;			/* Year	- 1900.  */
  int tm_yday;			/* Days in year.[1-366]	*/
};

void irigb_init(){

	IRIGB = (volatile struct IRIGB_WB *)BASE_IRIG_B;

	//IRIG-B; Enable ON, Reset OFF & Output OFF.
	//IRIGB->STAT = 0x2;
	IRIGB->STAT = (~IRIGB_STAT_RES & ~IRIGB_STAT_SET_IRIGB) & IRIGB_STAT_ENA;
	mprintf("IRIG-B init .... done. \n");
}

void irigb_format_time(uint64_t sec, struct irig_tm* t)
{

	unsigned long dayno;
	unsigned long long dayclock;
	int year = EPOCH_YR;
	int utc2tai;


	if ( sec > 35 )
		utc2tai = ext_utc_offset;
	else
		utc2tai = 0;

	dayclock = (unsigned long long)((sec-utc2tai) % SECS_DAY);
	dayno = (unsigned long)(sec / SECS_DAY);


	t->tm_sec = (dayclock % 60);
	t->tm_min = ((dayclock % 3600) / 60);
	t->tm_hour = (dayclock / 3600);
	while (dayno >= YEARSIZE(year)) {
		dayno -= YEARSIZE(year);
		year++;
	}
	t->tm_year = year - YEAR0;
	t->tm_yday = dayno+1; //dayno goes from 0 to 365, we need from 1 to 366
}


void irigbsync()
{
	uint32_t value=0;
	uint64_t sec;
	uint32_t nsec;
	struct irig_tm t;

	shw_pps_gen_get_time(&sec, &nsec);

	irigb_format_time(sec,&t);

	IRIGB->YEA = t.tm_year + YEAR0;
	IRIGB->DAY = t.tm_yday;
	IRIGB->HOU = t.tm_hour;
	IRIGB->MIN = t.tm_min;
	IRIGB->SEC = t.tm_sec;
}

//Function to print the IRIG-B register

void irigb_print(){
	mprintf("Status reg: 0x%8x .",IRIGB->STAT);
	mprintf("Year: %i .", IRIGB->YEA);
	mprintf("Day: %i .",  IRIGB->DAY);
	mprintf("Hour: %i .", IRIGB->HOU);
	mprintf("Mins: %i .", IRIGB->MIN);
	mprintf("Secs: %i . \n", IRIGB->SEC);
	mprintf("UTC to TAI (leap seconds): %i \n", ext_utc_offset);
}

//Function to check the IRIG-B state.

int return_irigb_state() {
	return (( IRIGB->STAT & IRIGB_STAT_SET_IRIGB ) == IRIGB_STAT_SET_IRIGB );
}
//Function to enable/disable the IRIG-B output at the front panel (SMA).

int irigb_output(int enable){

	if(enable)
		IRIGB->STAT |= IRIGB_STAT_SET_IRIGB;
	else
		IRIGB->STAT &= ~IRIGB_STAT_SET_IRIGB;

	irigbsync();

	return 1;
}
