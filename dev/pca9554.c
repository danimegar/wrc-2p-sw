#include "pca9554.h"

unsigned char read_i2c_pca9554(uint8_t i2cif, unsigned char addr) {
	unsigned char d;
	
	mi2c_start(i2cif);
	mi2c_put_byte(i2cif, I2C_ADDR_TO_WRITE(addr));
	mi2c_put_byte(i2cif, PCA9554_INPUT_REG);
	mi2c_repeat_start(i2cif);
	mi2c_put_byte(i2cif, I2C_ADDR_TO_READ(addr));
	mi2c_get_byte(i2cif,&d,1);
	mi2c_stop(i2cif);
	
	return d;
}

void write_i2c_pca9554(uint8_t i2cif, unsigned char addr, unsigned char data) {
	mi2c_start(i2cif);
	mi2c_put_byte(i2cif, I2C_ADDR_TO_WRITE(addr));
	mi2c_put_byte(i2cif, PCA9554_OUTPUT_REG);
	mi2c_put_byte(i2cif, data);
	mi2c_stop(i2cif);
}

void configure_i2c_pca9554(uint8_t i2cif, unsigned char addr, unsigned char data) {
	mi2c_start(i2cif);
	mi2c_put_byte(i2cif, I2C_ADDR_TO_WRITE(addr));
	mi2c_put_byte(i2cif, PCA9554_CONFIG_REG);
	mi2c_put_byte(i2cif, data);
	mi2c_stop(i2cif);
}
