/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2016 (Based on previous works from the wr-switch-sw)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Alessandro Rubini <rubini@gnudd.com>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

/*
 * SPI sw controller to handle the WR-LEN Expansion SPI peripheral.
 *
 * SPI stuff, used by later code
 */

#define SPI_REG_RX0	0
#define SPI_REG_RX1	4
#define SPI_REG_RX2	8
#define SPI_REG_RX3	12

#define SPI_REG_TX0	0
#define SPI_REG_TX1	4
#define SPI_REG_TX2	8
#define SPI_REG_TX3	12

#define SPI_REG_CTRL	16
#define SPI_REG_DIVIDER	20
#define SPI_REG_SS		24

#define SPI_CTRL_ASS			(1<<13)
#define SPI_CTRL_IE				(1<<12)
#define SPI_CTRL_LSB			(1<<11)
#define SPI_CTRL_TXNEG			(1<<10)
#define SPI_CTRL_RXNEG			(1<<9)
#define SPI_CTRL_GO_BSY			(1<<8)
#define SPI_CTRL_CHAR_LEN(x)	((x) & 0x7f)	/* 128 bits are transmitted in one transfer */

#define CS_PLL	0 /* Expansion SPI on CS0 */

/* Calls to fucntions */

void exp_spi_init();
void exp_spi_write_reg(uint8_t reg, uint8_t val);
uint8_t exp_spi_read_reg(uint8_t reg);

