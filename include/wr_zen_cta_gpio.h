#ifndef WR_ZEN_CTA_GPIO_H
#define WR_ZEN_CTA_GPIO_H

#include <wrc.h>

#define N_FPGA_GPIO_BANK 32
#define N_GPIOS 2

#define WB_GPIO_GTP_RESET 0x0
#define WB_GPIO_SW_RESET  0x1

#define FPGA_GPIO_CLR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20)
#define FPGA_GPIO_SET_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x4)
#define FPGA_GPIO_DIR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x8)
#define FPGA_GPIO_STA_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0xc)

#define FPGA_GPIO_SET_DATA(N) (0x1 << (N%N_FPGA_GPIO_BANK))
#define FPGA_GPIO_CLR_DATA(N) (FPGA_GPIO_SET_DATA(N))

void wr_zen_gpio_gtp_rst();
void wr_zen_gpio_init();

#endif
