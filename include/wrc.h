#ifndef __WRC_H__
#define __WRC_H__

/*
 * This header includes all generic prototypes that were missing
 * in earlier implementations. For example, the monitor is only
 * one function and doesn't deserve an header of its own.
 * Also, this brings in very common and needed headers
 */
#include <inttypes.h>
#include <syscon.h>
#include <pp-printf.h>
#define mprintf pp_printf
#define vprintf pp_vprintf
#define sprintf pp_sprintf

#undef offsetof
#define offsetof(TYPE, MEMBER) ((int) &((TYPE *)0)->MEMBER)
#undef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

void wrc_mon_gui(void);
void wrc_init_stats();
void wrc_dump_refresh(uint8_t force);
void shell_init(void);
int wrc_log_stats(uint8_t onetime);
void wrc_debug_printf(int subsys, const char *fmt, ...);

/* Default width (in 8ns units) of the pulses on the PPS output */
#define PPS_WIDTH (10 * 1000 * 1000 / 8) /* 10ms */

/* The time to refresh the stat command must be shared by cmd_stat.c and the monitor_*.c */
extern int ext_refresh_time;

/* The compilation board */
extern char board_name_ext[13];

/* UTC to TAI difference */
extern int ext_utc_offset;
extern int ext_utc_offset_updated;

//WR-LEN
//Ports defined for the WR-LEN board.
#define wrlen_num_ports	 2

//WR-LEN
#define PORT_STATS 0

//WR-LEN
//Choose the slave port for the wr-len board. 0 means Port1 and 1 Port2.
#define LEN_PORT_IN_USE	 0

/* This is in the library, somewhere */
extern int abs(int val);

/* The following from ptp-noposix */
extern void wr_servo_reset(void);
void update_rx_queues(int port);

struct wrc_stats {
	char magic[8];
	void* spll_off;
	void* servo_off;
	void* ps0_off;
	void* ps1_off;
};

extern struct wrc_stats *wrc_stats_head;


#endif /* __WRC_H__ */
