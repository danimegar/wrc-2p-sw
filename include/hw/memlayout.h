#ifndef __REGS_H
#define __REGS_H

//WR-D-P
#define SDB_ADDRESS 0x70000		//Two contiguous RAMs

//WR-D-P
extern unsigned char *BASE_MINIC[2];            // x 2 ports
extern unsigned char *BASE_EP[2];               // x 2 ports

extern unsigned char *BASE_SOFTPLL;
extern unsigned char *BASE_PPS_GEN;
extern unsigned char *BASE_SYSCON;
extern unsigned char *BASE_UART;
extern unsigned char *BASE_ONEWIRE;
extern unsigned char *BASE_GW_VERSION;

#ifdef CONFIG_ETHERBONE
	extern unsigned char *BASE_ETHERBONE_CFG;
#endif

#ifdef CONFIG_IRIGB
	extern unsigned char *BASE_IRIG_B;		//IRIG-B module.
#endif

#ifdef CONFIG_ETHPORT
	extern unsigned char *BASE_WB_ETHSUB;		//Ethernet Subsystem module. It's connected via the  wb_axi_bridge
	extern unsigned char *BASE_WB_MINISWCH;
#endif

#ifdef CONFIG_DIO
	extern unsigned char *BASE_WB_DIO;
#endif

#ifdef CONFIG_EXPANSION_SPI
	extern unsigned char *BASE_WB_EXPANSION_SPI;
#endif

#if defined(CONFIG_ZEN_BOARD) || defined(CONFIG_ZEN_CTA_BOARD) || defined(CONFIG_DIO)
	extern unsigned char *BASE_WB_GPIO;

#ifdef  CONFIG_LENTRIG
    extern unsigned char *BASE_WB_LENTRIG;
#endif

#endif

#ifdef CONFIG_XADC_TEMP
    extern unsigned char *BASE_XADC_TEMP;
#endif

#if defined(CONFIG_ZEN_BOARD) || defined(CONFIG_ZEN_CTA_BOARD)
	#define FMC_EEPROM_ADR 0x51 //WR-LEN: S101 0101 (A1=GND). (SPEC=0x50) (LEN=0x55) (ZEN && ZEN_CTA=0x51)
#elif CONFIG_LEN_BOARD
	#define FMC_EEPROM_ADR 0x55 //WR-LEN: S101 0101 (A1=GND). (SPEC=0x50) (LEN=0x55) (ZEN && ZEN_CTA=0x51)
#else
	#define FMC_EEPROM_ADR 0x55 //WR-LEN: S101 0101 (A1=GND). (SPEC=0x50) (LEN=0x55) (ZEN && ZEN_CTA=0x51)
#endif

void sdb_find_devices(void);
void sdb_print_devices(void);

#endif
