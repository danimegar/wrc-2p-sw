/*
Command to launch the ETH RTU printf function.
eml. emilio<AT>sevensols.com
*/

#include <stdlib.h>     /* strtol */
#include "shell.h"
#include <string.h>

#include "mini_switch_core.h"

static int cmd_eth(const char *args[]){
	ms_eth_print_rtu();
	return 0;
}

DEFINE_WRC_COMMAND(eth) = {
	.name = "eth",
	.exec = cmd_eth,
};
