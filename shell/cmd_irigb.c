/*
Command to enable/disable the IRIG-B output at the front panel (SMA).
eml. emilio<AT>sevensols.com
*/
#include <stdlib.h>     /* strtol */
#include "shell.h"
#include <wrpc.h>


#include "irigb_sync.h"	//Irig-b module

extern int ptp_mode;

static int cmd_irigb(const char *args[])
{
	if (!args[0]) {

		if(return_irigb_state() == 1)
			mprintf("on \n");
		else
			mprintf("off \n");


	} else if (!strcasecmp(args[0], "on")) {

		if ( ptp_mode != WRC_MODE_GM)
		{
			if (irigb_output(1) < 0)
				mprintf("Enabling ... error. \n");
			else
				mprintf("Enabling ... done. \n");
		} else
			mprintf("The LEN board is set to GM mode. PPS is input. IRIG-B disabled \n");

	} else if (!strcasecmp(args[0], "off")) {

		if (irigb_output(0) < 0)
			mprintf("Disabling ... error. \n");
		else
			mprintf("Disabling ... done. \n");

	} else if (!strcasecmp(args[0], "print")) {
		irigb_print();

	} else {
	     mprintf("Wrong irigb call! \n");
	     mprintf(">irigb on \n");
	     mprintf(">irigb off \n");
	     mprintf(">irigb print \n");
	}
}



DEFINE_WRC_COMMAND(irigb) = {
	.name = "irigb",
	.exec = cmd_irigb,
};
