/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2012 CERN (www.cern.ch)
 * Author: Grzegorz Daniluk <grzegorz.daniluk@cern.ch>
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

/* 	Command: calibration
		Arguments: [force]

		Description: launches RX timestamper calibration. */

#include <string.h>
#include <wrc.h>
#include "shell.h"
#include "eeprom.h"
#include "syscon.h"
#include "rxts_calibrator.h"

static int cmd_calibration(const char *args[])
{
	uint32_t trans;
	char *ifname;
	uint8_t port;
	ifname = args[0];

	if (strcasecmp(ifname, "wr0") && strcasecmp(ifname, "wr1")){
		mprintf("specify:\n");
		mprintf("	- wr0 or wr1 port. \n");
		mprintf("	- Forcing the calibration\n\n");
		mprintf("\ncalibration <port> <force> \n\n");
		return -1;
	}else
		port = atoi(&ifname[2]) ;

	if (args[1] && !strcasecmp(args[1], "force")) {
		if (measure_t24p(&trans, port) < 0)
			return -1;

		//restart the PTP demon.
		wrc_ptp_start();

		return eeprom_phtrans(WRPC_FMC_I2C, FMC_EEPROM_ADR, &trans, 1, port);


	} else if (!args[1]) {
		if (eeprom_phtrans(WRPC_FMC_I2C, FMC_EEPROM_ADR, &trans, 0, port) > 0) {
			mprintf("Found phase transition in EEPROM: %dps\n",
				trans);
			cal_phase_transition[port] = trans;
			return 0;
		} else {
			mprintf("Measuring t2/t4 phase transition...\n");
			if (measure_t24p(&trans, port) < 0)
				return -1;
			cal_phase_transition[port] = trans;
			return eeprom_phtrans(WRPC_FMC_I2C, FMC_EEPROM_ADR,
					      &trans, 1, port);
		}
	} else {
		mprintf("specify:\n");
		mprintf("	- wr0 or wr1 port. \n");
		mprintf("	- Forcing the calibration\n\n");
		mprintf("\ncalibration <port> <force> \n\n");
		return -1;
	}

	return 0;
}

DEFINE_WRC_COMMAND(calibration) = {
	.name = "calibration",
	.exec = cmd_calibration,
};
