#ifndef IPV4_H
#define IPV4_H

#include <../include/wrc.h>
#include <inttypes.h>

void ipv4_init(const char *if_name);
void ipv4_poll(int port);

/* Internal to IP stack: */
unsigned int ipv4_checksum(unsigned short *buf, int shorts);

void arp_init(const char *if_name);
void arp_poll(int port);

extern int needIP;

void getIP(unsigned char *IP);
void setIP(unsigned char *IP);

int process_icmp(uint8_t * buf, int len);
int process_bootp(uint8_t * buf, int len, int port);	/* non-zero if IP was set */
int send_bootp(uint8_t * buf, int retry, int port);

#endif
