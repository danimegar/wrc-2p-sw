/*
 * dump-info.c
 *
 * @warning: This file is a backport of wrpc-sw-v3.0 to wrc-2p-sw
 *
 *  Created on: Sep 6, 2016
 *      Author: Benoit Rat
 */


#include <sys/types.h>
#include <ppsi/ppsi.h>
#include <softpll_ng.h>
#include <hal_exports.h>

#include "dump-info.h"

struct dump_info  dump_info[] = {

		/* map for fields of ppsi structures */
#undef DUMP_STRUCT
#define DUMP_STRUCT hexp_port_state_t

	DUMP_HEADER("port_state"),
	DUMP_FIELD(int,valid),
	DUMP_FIELD(int,mode),
	DUMP_FIELD(UInteger32,delta_tx),
	DUMP_FIELD(UInteger32,delta_rx),
	DUMP_FIELD(UInteger32,phase_val),
	DUMP_FIELD(int,phase_val_valid),
	DUMP_FIELD(int,up),
	DUMP_FIELD(int,tx_calibrated),
	DUMP_FIELD(int,rx_calibrated),
	DUMP_FIELD(int,tx_tstamp_counter),
	DUMP_FIELD(int,rx_tstamp_counter),
	DUMP_FIELD(int,is_locked),
	DUMP_FIELD(int,lock_priority),
	DUMP_FIELD(UInteger32,phase_setpoint),
	DUMP_FIELD(UInteger32,clock_period),
	DUMP_FIELD(UInteger32,t2_phase_transition),
	DUMP_FIELD(UInteger32,t4_phase_transition),
	DUMP_FIELD_SIZE(bina, hw_addr, 6),
	DUMP_FIELD(int,hw_index),
	DUMP_FIELD(int,fiber_fix_alpha),
	DUMP_FIELD(int,tx_count),
	DUMP_FIELD(int,rx_count),


/* map for fields of ppsi structures */
#undef DUMP_STRUCT
#define DUMP_STRUCT ptpdexp_sync_state_t

	DUMP_HEADER("servo_state"),
	DUMP_FIELD(int,valid),
	DUMP_FIELD_SIZE(char, slave_servo_state, 32),
	DUMP_FIELD_SIZE(char, sync_source, 32),
	DUMP_FIELD(int, tracking_enabled),		/* FIXME: follow this */
	DUMP_FIELD(Integer64,delay_ms),
	DUMP_FIELD(Integer64,delta_tx_m),
	DUMP_FIELD(Integer64,delta_rx_m),
	DUMP_FIELD(Integer64,delta_tx_s),
	DUMP_FIELD(Integer64,delta_rx_s),
	DUMP_FIELD(Integer64,fiber_asymmetry),
	DUMP_FIELD(Integer64,total_asymmetry),
	DUMP_FIELD(Integer64,cur_offset),
	DUMP_FIELD(Integer64,cur_setpoint),
	DUMP_FIELD(Integer64,cur_skew),
	DUMP_FIELD(Integer64,update_count),
	DUMP_FIELD(Integer32,mode),
	DUMP_FIELD(UInteger64,sec),
	DUMP_FIELD(UInteger32,nsec),
	DUMP_FIELD(Integer16,temp),
	DUMP_FIELD(UInteger16,temp16),


#undef DUMP_STRUCT
#define DUMP_STRUCT struct softpll_state

	DUMP_HEADER("softpll"),
	DUMP_FIELD(int, mode),
	DUMP_FIELD(int, seq_state),
	DUMP_FIELD(int, dac_timeout),
	DUMP_FIELD(int, default_dac_main),
	DUMP_FIELD(int, delock_count),
	//DUMP_FIELD(uint32_t, irq_count),
	DUMP_FIELD(int, mpll_shift_ps),
	DUMP_FIELD(int, helper.p_adder),
	DUMP_FIELD(int, helper.p_setpoint),
	DUMP_FIELD(int, helper.tag_d0),
	DUMP_FIELD(int, helper.ref_src),
	DUMP_FIELD(int, helper.sample_n),
	DUMP_FIELD(int, helper.delock_count),
	/* FIXME: missing helper.pi etc.. */
	DUMP_FIELD(int, ext.enabled),
	DUMP_FIELD(int, ext.align_state),
	DUMP_FIELD(int, ext.align_timer),
	DUMP_FIELD(int, ext.align_target),
	DUMP_FIELD(int, ext.align_step),
	DUMP_FIELD(int, ext.align_shift),
	DUMP_FIELD(int, mpll.state),
	/* FIXME: mpll.pi etc */
	DUMP_FIELD(int, mpll.adder_ref),
	DUMP_FIELD(int, mpll.adder_out),
	DUMP_FIELD(int, mpll.tag_ref),
	DUMP_FIELD(int, mpll.tag_out),
	DUMP_FIELD(int, mpll.tag_ref_d),
	DUMP_FIELD(int, mpll.tag_out_d),
	DUMP_FIELD(uint32_t, mpll.seq_ref),
	DUMP_FIELD(int, mpll.seq_out),
	DUMP_FIELD(int, mpll.match_state),
	DUMP_FIELD(int, mpll.match_seq),
	DUMP_FIELD(int, mpll.phase_shift_target),
	DUMP_FIELD(int, mpll.phase_shift_current),
	DUMP_FIELD(int, mpll.id_ref),
	DUMP_FIELD(int, mpll.id_out),
	DUMP_FIELD(int, mpll.sample_n),
	DUMP_FIELD(int, mpll.delock_count),
	DUMP_FIELD(int, mpll.dac_index),
	DUMP_FIELD(int, mpll.enabled),

	DUMP_HEADER("end"),

};


