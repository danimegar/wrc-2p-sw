################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../ppsi/proto-ext-whiterabbit/fsm-table.o \
../ppsi/proto-ext-whiterabbit/hooks.o \
../ppsi/proto-ext-whiterabbit/libwr.o \
../ppsi/proto-ext-whiterabbit/state-wr-calibrated.o \
../ppsi/proto-ext-whiterabbit/state-wr-calibration.o \
../ppsi/proto-ext-whiterabbit/state-wr-link-on.o \
../ppsi/proto-ext-whiterabbit/state-wr-locked.o \
../ppsi/proto-ext-whiterabbit/state-wr-m-lock.o \
../ppsi/proto-ext-whiterabbit/state-wr-present.o \
../ppsi/proto-ext-whiterabbit/state-wr-resp-calib-req.o \
../ppsi/proto-ext-whiterabbit/state-wr-s-lock.o \
../ppsi/proto-ext-whiterabbit/wr-msg.o \
../ppsi/proto-ext-whiterabbit/wr-servo.o 

C_SRCS += \
../ppsi/proto-ext-whiterabbit/fsm-table.c \
../ppsi/proto-ext-whiterabbit/hooks.c \
../ppsi/proto-ext-whiterabbit/state-wr-calibrated.c \
../ppsi/proto-ext-whiterabbit/state-wr-calibration.c \
../ppsi/proto-ext-whiterabbit/state-wr-link-on.c \
../ppsi/proto-ext-whiterabbit/state-wr-locked.c \
../ppsi/proto-ext-whiterabbit/state-wr-m-lock.c \
../ppsi/proto-ext-whiterabbit/state-wr-present.c \
../ppsi/proto-ext-whiterabbit/state-wr-resp-calib-req.c \
../ppsi/proto-ext-whiterabbit/state-wr-s-lock.c \
../ppsi/proto-ext-whiterabbit/wr-msg.c \
../ppsi/proto-ext-whiterabbit/wr-servo.c 

OBJS += \
./ppsi/proto-ext-whiterabbit/fsm-table.o \
./ppsi/proto-ext-whiterabbit/hooks.o \
./ppsi/proto-ext-whiterabbit/state-wr-calibrated.o \
./ppsi/proto-ext-whiterabbit/state-wr-calibration.o \
./ppsi/proto-ext-whiterabbit/state-wr-link-on.o \
./ppsi/proto-ext-whiterabbit/state-wr-locked.o \
./ppsi/proto-ext-whiterabbit/state-wr-m-lock.o \
./ppsi/proto-ext-whiterabbit/state-wr-present.o \
./ppsi/proto-ext-whiterabbit/state-wr-resp-calib-req.o \
./ppsi/proto-ext-whiterabbit/state-wr-s-lock.o \
./ppsi/proto-ext-whiterabbit/wr-msg.o \
./ppsi/proto-ext-whiterabbit/wr-servo.o 

C_DEPS += \
./ppsi/proto-ext-whiterabbit/fsm-table.d \
./ppsi/proto-ext-whiterabbit/hooks.d \
./ppsi/proto-ext-whiterabbit/state-wr-calibrated.d \
./ppsi/proto-ext-whiterabbit/state-wr-calibration.d \
./ppsi/proto-ext-whiterabbit/state-wr-link-on.d \
./ppsi/proto-ext-whiterabbit/state-wr-locked.d \
./ppsi/proto-ext-whiterabbit/state-wr-m-lock.d \
./ppsi/proto-ext-whiterabbit/state-wr-present.d \
./ppsi/proto-ext-whiterabbit/state-wr-resp-calib-req.d \
./ppsi/proto-ext-whiterabbit/state-wr-s-lock.d \
./ppsi/proto-ext-whiterabbit/wr-msg.d \
./ppsi/proto-ext-whiterabbit/wr-servo.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/proto-ext-whiterabbit/%.o: ../ppsi/proto-ext-whiterabbit/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


