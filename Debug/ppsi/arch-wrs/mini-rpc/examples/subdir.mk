################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-wrs/mini-rpc/examples/freestanding-server.c \
../ppsi/arch-wrs/mini-rpc/examples/mbox-bridge.c \
../ppsi/arch-wrs/mini-rpc/examples/mbox-client.c \
../ppsi/arch-wrs/mini-rpc/examples/mbox-process.c \
../ppsi/arch-wrs/mini-rpc/examples/pty-client.c \
../ppsi/arch-wrs/mini-rpc/examples/pty-rpc_server.c \
../ppsi/arch-wrs/mini-rpc/examples/pty-rpc_structs.c \
../ppsi/arch-wrs/mini-rpc/examples/pty-server.c \
../ppsi/arch-wrs/mini-rpc/examples/shmem-client.c \
../ppsi/arch-wrs/mini-rpc/examples/shmem-server.c \
../ppsi/arch-wrs/mini-rpc/examples/shmem-structs.c \
../ppsi/arch-wrs/mini-rpc/examples/trivial-client.c \
../ppsi/arch-wrs/mini-rpc/examples/trivial-server.c 

OBJS += \
./ppsi/arch-wrs/mini-rpc/examples/freestanding-server.o \
./ppsi/arch-wrs/mini-rpc/examples/mbox-bridge.o \
./ppsi/arch-wrs/mini-rpc/examples/mbox-client.o \
./ppsi/arch-wrs/mini-rpc/examples/mbox-process.o \
./ppsi/arch-wrs/mini-rpc/examples/pty-client.o \
./ppsi/arch-wrs/mini-rpc/examples/pty-rpc_server.o \
./ppsi/arch-wrs/mini-rpc/examples/pty-rpc_structs.o \
./ppsi/arch-wrs/mini-rpc/examples/pty-server.o \
./ppsi/arch-wrs/mini-rpc/examples/shmem-client.o \
./ppsi/arch-wrs/mini-rpc/examples/shmem-server.o \
./ppsi/arch-wrs/mini-rpc/examples/shmem-structs.o \
./ppsi/arch-wrs/mini-rpc/examples/trivial-client.o \
./ppsi/arch-wrs/mini-rpc/examples/trivial-server.o 

C_DEPS += \
./ppsi/arch-wrs/mini-rpc/examples/freestanding-server.d \
./ppsi/arch-wrs/mini-rpc/examples/mbox-bridge.d \
./ppsi/arch-wrs/mini-rpc/examples/mbox-client.d \
./ppsi/arch-wrs/mini-rpc/examples/mbox-process.d \
./ppsi/arch-wrs/mini-rpc/examples/pty-client.d \
./ppsi/arch-wrs/mini-rpc/examples/pty-rpc_server.d \
./ppsi/arch-wrs/mini-rpc/examples/pty-rpc_structs.d \
./ppsi/arch-wrs/mini-rpc/examples/pty-server.d \
./ppsi/arch-wrs/mini-rpc/examples/shmem-client.d \
./ppsi/arch-wrs/mini-rpc/examples/shmem-server.d \
./ppsi/arch-wrs/mini-rpc/examples/shmem-structs.d \
./ppsi/arch-wrs/mini-rpc/examples/trivial-client.d \
./ppsi/arch-wrs/mini-rpc/examples/trivial-server.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-wrs/mini-rpc/examples/%.o: ../ppsi/arch-wrs/mini-rpc/examples/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


