################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-wrs/mini-rpc/minipc-client.c \
../ppsi/arch-wrs/mini-rpc/minipc-core.c \
../ppsi/arch-wrs/mini-rpc/minipc-mem-server.c \
../ppsi/arch-wrs/mini-rpc/minipc-server.c 

OBJS += \
./ppsi/arch-wrs/mini-rpc/minipc-client.o \
./ppsi/arch-wrs/mini-rpc/minipc-core.o \
./ppsi/arch-wrs/mini-rpc/minipc-mem-server.o \
./ppsi/arch-wrs/mini-rpc/minipc-server.o 

C_DEPS += \
./ppsi/arch-wrs/mini-rpc/minipc-client.d \
./ppsi/arch-wrs/mini-rpc/minipc-core.d \
./ppsi/arch-wrs/mini-rpc/minipc-mem-server.d \
./ppsi/arch-wrs/mini-rpc/minipc-server.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-wrs/mini-rpc/%.o: ../ppsi/arch-wrs/mini-rpc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


