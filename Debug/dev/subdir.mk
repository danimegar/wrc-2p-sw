################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../dev/eeprom.o \
../dev/endpoint.o \
../dev/ep_pfilter.o \
../dev/i2c.o \
../dev/minic.o \
../dev/pps_gen.o \
../dev/rxts_calibrator.o \
../dev/sdb.o \
../dev/sfp.o \
../dev/syscon.o \
../dev/uart.o \
../dev/w1-eeprom.o \
../dev/w1-hw.o \
../dev/w1-shell.o \
../dev/w1-temp.o \
../dev/w1.o 

C_SRCS += \
../dev/dna.c \
../dev/eeprom.c \
../dev/endpoint.c \
../dev/ep_pfilter.c \
../dev/i2c.c \
../dev/minic.c \
../dev/pps_gen.c \
../dev/rxts_calibrator.c \
../dev/sdb-eeprom.c \
../dev/sdb.c \
../dev/sfp.c \
../dev/syscon.c \
../dev/uart-sw.c \
../dev/uart.c \
../dev/w1-eeprom.c \
../dev/w1-hw.c \
../dev/w1-shell.c \
../dev/w1-temp.c \
../dev/w1.c 

OBJS += \
./dev/dna.o \
./dev/eeprom.o \
./dev/endpoint.o \
./dev/ep_pfilter.o \
./dev/i2c.o \
./dev/minic.o \
./dev/pps_gen.o \
./dev/rxts_calibrator.o \
./dev/sdb-eeprom.o \
./dev/sdb.o \
./dev/sfp.o \
./dev/syscon.o \
./dev/uart-sw.o \
./dev/uart.o \
./dev/w1-eeprom.o \
./dev/w1-hw.o \
./dev/w1-shell.o \
./dev/w1-temp.o \
./dev/w1.o 

C_DEPS += \
./dev/dna.d \
./dev/eeprom.d \
./dev/endpoint.d \
./dev/ep_pfilter.d \
./dev/i2c.d \
./dev/minic.d \
./dev/pps_gen.d \
./dev/rxts_calibrator.d \
./dev/sdb-eeprom.d \
./dev/sdb.d \
./dev/sfp.d \
./dev/syscon.d \
./dev/uart-sw.d \
./dev/uart.d \
./dev/w1-eeprom.d \
./dev/w1-hw.d \
./dev/w1-shell.d \
./dev/w1-temp.d \
./dev/w1.d 


# Each subdirectory must supply rules for building sources it contributes
dev/%.o: ../dev/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


