################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tools/lm32-etheruart/lm32-etheruart.c \
../tools/lm32-etheruart/simple-eb.c 

OBJS += \
./tools/lm32-etheruart/lm32-etheruart.o \
./tools/lm32-etheruart/simple-eb.o 

C_DEPS += \
./tools/lm32-etheruart/lm32-etheruart.d \
./tools/lm32-etheruart/simple-eb.d 


# Each subdirectory must supply rules for building sources it contributes
tools/lm32-etheruart/%.o: ../tools/lm32-etheruart/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


