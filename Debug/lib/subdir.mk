################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../lib/arp.o \
../lib/atoi.o \
../lib/icmp.o \
../lib/ipv4.o \
../lib/net.o \
../lib/ppsi-wrappers.o \
../lib/ptp-noposix-wrappers.o \
../lib/usleep.o \
../lib/util.o 

C_SRCS += \
../lib/arp.c \
../lib/atoi.c \
../lib/bootp.c \
../lib/icmp.c \
../lib/ipv4.c \
../lib/net.c \
../lib/ppsi-wrappers.c \
../lib/ptp-noposix-wrappers.c \
../lib/usleep.c \
../lib/util.c 

OBJS += \
./lib/arp.o \
./lib/atoi.o \
./lib/bootp.o \
./lib/icmp.o \
./lib/ipv4.o \
./lib/net.o \
./lib/ppsi-wrappers.o \
./lib/ptp-noposix-wrappers.o \
./lib/usleep.o \
./lib/util.o 

C_DEPS += \
./lib/arp.d \
./lib/atoi.d \
./lib/bootp.d \
./lib/icmp.d \
./lib/ipv4.d \
./lib/net.d \
./lib/ppsi-wrappers.d \
./lib/ptp-noposix-wrappers.d \
./lib/usleep.d \
./lib/util.d 


# Each subdirectory must supply rules for building sources it contributes
lib/%.o: ../lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


