################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../softpll/softpll_ng.o \
../softpll/spll_common.o \
../softpll/spll_external.o \
../softpll/spll_helper.o \
../softpll/spll_main.o \
../softpll/spll_ptracker.o 

C_SRCS += \
../softpll/softpll_ng.c \
../softpll/spll_common.c \
../softpll/spll_external.c \
../softpll/spll_helper.c \
../softpll/spll_main.c \
../softpll/spll_ptracker.c 

OBJS += \
./softpll/softpll_ng.o \
./softpll/spll_common.o \
./softpll/spll_external.o \
./softpll/spll_helper.o \
./softpll/spll_main.o \
./softpll/spll_ptracker.o 

C_DEPS += \
./softpll/softpll_ng.d \
./softpll/spll_common.d \
./softpll/spll_external.d \
./softpll/spll_helper.d \
./softpll/spll_main.d \
./softpll/spll_ptracker.d 


# Each subdirectory must supply rules for building sources it contributes
softpll/%.o: ../softpll/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


